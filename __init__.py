# coding: utf-8

"""
Задача:
V список самых распространенных слов в тексте;
кол-во слов короче 3-х символов;
кол-во слов длиннее 9-и символов;
найти самое длинное предложение;
найти самое короткое предложение;
найти предложение в котором самое большое кол-во запятых и других знаков препинания;
"""

############################### ПАРСЕР ###################################

from collections import Counter
import re
import os
import sys
reload(sys)
sys.setdefaultencoding('utf8')
import chardet
import codecs


def file_code_detect(text_file_path):
    """
    Функция примерного определения кодировки указанного файла
    :param text_file_path: string [Принимает путь до файла]
    :return: string [Возвращает кодировку файла]
    """
    file = open(text_file_path).read()
    return chardet.detect(file)['encoding']

def load_file(file_name):
    """
    Функция чтения из файла
    :param file_name string: путь до файла
    :return string: текст
    """
    # Определяем кодировку указанного файла
    file_code = file_code_detect(file_name)
    # Открываем файл
    text_buffer = codecs.open(file_name, 'r', file_code).read()
    # Возвращаем прочитанный текст
    return text_buffer

def get_popular_words(text_bufer):
    """
    Данная функция считает количество повторяющихся слов (слово должно встречатся боллее 1 раза)
    и возвращает список кортежей:
    ('слово', количество повторений)
    :param (string) text_bufer: считаный файл в unicode
    :return (list[tuple]): Список с кортежами
    """
    # Считываем из переданной unicode строки слова с помощью регулярного вырожения
    word_array = re.findall(r'[\w*]+[^\s*|\W*]', text_bufer, re.U | re.I | re.M)
    # С помощью модуля Counter создаем список из 10 слов встречающихся в тексте не менее 1 раза
    pre_popular_words = Counter(word_array).most_common(10)
    # Если есть слова повторяющиеся в тексте более 1 раза,
    # то удаляем из списка слова повторяющаися менее 2 раз
    if pre_popular_words[0] == '':
        popular_words = [item for item in pre_popular_words if item[1]>1]
    popular_words = pre_popular_words
    return popular_words

def get_words_len_s3(text_bufer):
    """
    Данная функция ищет все слова в тексте меньше 3
    :param text_bufer: считаный файл в unicode
    :return: список состоящий из слов длинная которых меньше 3
    """
    word_array = re.findall(r'[\w*]+[^\s*|\W*]', text_bufer, re.U | re.I | re.M)
    word_array_result = [item for item in word_array if len(item) < 3]
    return len(word_array_result)

def get_words_len_l9(text_bufer):
    """
    Данная функция ищет все слова в тексте больше 9
    :param text_bufer: считаный файл в unicode
    :return: список состоящий из слов длинная которых больше 9
    """
    word_array = re.findall(r'[\w*]+[^\s*|\W*]', text_bufer, re.U | re.I | re.M)
    word_array_result = [item for item in word_array if len(item) > 9]
    return len(word_array_result)

def get_max_min_znmax_predl(text_bufer):
    """
    Данная функция находит в тексте самое большое, самое маленькое предложение
    и предложение, содержащее в себе самое большое колчичество знаков препинания
    :param text_bufer:
    :return:
    """
    #list_predl = re.findall(r'[^\.]+[\n|\.|\!|\?]', text_bufer, re.U | re.I | re.M)
    list_predl = re.findall(r'\S[^\.\!\?]+[\.\!\?]', text_bufer, re.U | re.I | re.M)
    # Счетчик цикла
    i = 0
    # Присваеваем длине самого короткого предложения дли 0
    # элемента. если надем элементкороче, запишем его.
    # 0 сразу присвоить нельзя, т.к. може быть текст без
    # пустых строк
    short_predl_len = len(list_predl[0])
    # Индекс самого короткого предложения
    short_predl_id = 0

    # Длина самого длинного предложения
    long_predl_len = 0
    # Наибольшее количество знаков препинания
    zn_prep_count = 0
    # Индекс предложения с наибольшим количеством знаков препинания
    zn_prep_index = 0

    while i < len(list_predl):
        # Длина текущего предложения
        current_len = len(list_predl[i])
        # Самое длинное предложение
        if current_len > long_predl_len:
            long_predl_len = current_len
            long_predl_id = i
        # Самое короткое предложение
        if current_len < short_predl_len:
            short_predl_len = current_len
            short_predl_id = i
        # Считаем знаки препинания в текущем предложении
        zn_prep = re.findall(r'[\:\;\"\'\.\,\-\!\?]', list_predl[i], re.U | re.I | re.M)
        # Предложение с наибольшим количеством знаков препинания
        if zn_prep_count < len(zn_prep):
            zn_prep_count = len(zn_prep)
            zn_prep_index = i
        i += 1
        # Возвращаем самые короткое и длинное предложения, предложение с
        # наибольшим количеством  знаков  препинания,  количество знаков
        # препинания
    result = [list_predl[short_predl_id], list_predl[long_predl_id],
              list_predl[zn_prep_index] , zn_prep_count]
    return result


######################### ИНТЕРФЕЙС #######################################
# Путь к файлу
text_file_path = ''

# Блок тестирование принятия аргументов программой
# print(sys.argv, '-p' in sys.argv) # Массив аргументов
# print(os.getcwd()) # Текущая директория

# Блок тестирование табличного вывода

while True:
    # Проверка на передачу ключа справки
    if ('--help' in sys.argv):
        print(U'Для запуска приложения введите:')
        print(U'\tpython2.7 __init__.py <путь_до_текстового_файла>\n')
        print(U'Пример: python2.7 __init__.py ~/Documents/Test_TextFile.txt\n')
        break
    # Проверяем передан ли путь к файлу
    elif len(sys.argv) > 1:
        print(U'Передан первый аргумент: {}'.format(sys.argv[1])) # Debag
        # Присваиваем переданный путь основной переменной пути
        text_file_path = os.path.abspath(sys.argv[1])
        # Проверяем файл на существование
        if os.path.isfile(text_file_path):
            print(U"Файл по пути {} существует".format(text_file_path)) # Debug
            # Проверяем файл на доступность чтения
            if os.access(text_file_path, os.R_OK):
                print(U"Файл по доступен для чтения") # Debug
                print(U"Начинаем парсинг файла...") # Debug
                text_bufer = load_file(text_file_path)
                if text_bufer:
                    is_create_html_report = raw_input("Обработка файла окончена. Хотите просмотреть отчет? [Y/n]").lower()
                    if (is_create_html_report == "n"):
                        print(U'Вы отказались от просмотра отчета, введя "{}"'.format(is_create_html_report))
                        print(U'Завершение программы.')
                    else:
                        print(U"\nМы нашли в этом файле следующее:\n")
                        print(U"Самые распостраненные слова в тексте:")
                        for words_item in get_popular_words(text_bufer):
                            print('Слово "{}" {} раз встречается в тексте'.format(words_item[0], words_item[1]))
                        print(U"Количество слов в которых менее 3-х символов: {}".format(get_words_len_s3(text_bufer)))
                        print(U"Количество слов в которых более 9-х символов: {}".format(get_words_len_l9(text_bufer)))
                        result = get_max_min_znmax_predl(text_bufer)
                        print(U"Самое короткое предложение:\n{}".format(result[0]))
                        print(U"Самое длинное предложение:\n{}".format(result[1]))
                        print(U'В придложении:\n"{}"\n{} знаков препинания'.format(result[2], result[3]))


            else:
                print(U"Файл НЕ доступен для чтения.\n")
                print(U"Либо откройте программу от имени пользователя которому доступно чтение, либо дайте файлу права "
                      U"с помощью команды (команду можно скопировать полностью):\n")
                print(U"sudo chmod 755 {}\n".format(text_file_path))
                print(U"Затем заного запустите программу с помощью команды:\n")
                print(U"python2.7 {}/__init__.py {}\n".format(os.getcwd(), text_file_path))
                print(U"Завершение программы.")

        else:
            print (U"Файл по пути {} не существует или не является файлом!".format(text_file_path))
            print (U"Завершение программы.")
        break
    else:
        print(U'Вы не передали путь к файлу. Для справки введите: __init__.py --help')
        break
